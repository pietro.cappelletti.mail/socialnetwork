// server.js
var bodyParser = require('body-parser')
var crypto = require('crypto');
const express = require('express');
const moment = require('moment');
const nodemailer = require("nodemailer");

const app = express();
app.use(bodyParser.json());
app.use(express.urlencoded());

////////////////////////////////////////////////////
//mail things
async function mailVerification(email, token){

  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
      user: 'sertrils.email@gmail.com', // generated ethereal user
      pass: '15973qwerty'// generated ethereal password
    }
  });

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"Sertrils | mail verification" <sertrils.email@gmail.com>', // sender address
    to: email, // list of receivers
    subject: "Verificate mail | Sertrils", // Subject line
    text: 'Verification link: http://localhost:1984/verificatemail.html?token=' + token, // plain text body
    html: '<b>Verification link: <a href="http://localhost:1984/verificatemail.html?token=' + token + '">ClickHere</a></b>' // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  // console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}
async function changepassword(email, token){

  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true, // true for 465, false for other ports
    auth: {
      user: 'sertrils.email@gmail.com', // generated ethereal user
      pass: '15973qwerty'// generated ethereal password
    }
  });

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"Sertrils | Request reset password" <sertrils.email@gmail.com>', // sender address
    to: email, // list of receivers
    subject: "Reset password | Sertrils", // Subject line
    text: 'Reset password link: http://localhost:1984/requestpassword.html?token=' + token, // plain text body
    html: '<b>Reset password link: <a href="http://localhost:1984/requestpassword.html?token=' + token + '">ClickHere</a></b>' // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}
//FORSE QUESTO POTREBBE CREARE ERRORE
// mailVerification().catch(console.error);
////////////////////////////////////////////////////

const PORT = process.env.PORT = 1984;

var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";

var databaseName = 'socialNetwork';
var collectionUtenti = 'users';
var userInfo = "userInfo";
var posts = "post";
var follower = "followers";
var messages = "messages";

//MongoClient connect indici

function randomString(length) {
   var result           = '';
   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
   var charactersLength = characters.length;
   for ( var i = 0; i < length; i++ ) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
   }
   return result;
}

console.log('\x1b[36m%s\x1b[0m',' \n- Sertrils Server ON\n');

//registerUtente(mail, password, rePassword, securityQuestion, username, securityAnswere, callback) --> [0] = R (true), R(false)
app.post('/db/registerUtente', function (req, res) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(databaseName);
    if (req.body.Password == req.body.rePassword) {
      dbo.collection(collectionUtenti).findOne({$or: [{ username: req.body.username },{ mail: req.body.mail }]} , function(err, result) {
        if (result != null) {
          console.log('Error: same mail or username[' + req.body.mail + ':' + req.body.username + ']');
          res.send('E');
        } else {
          var tokenRegister =randomString(30);
          var myobj = {username: req.body.username , mail: req.body.mail, password: crypto.createHash('sha256').update(req.body.Password).digest('hex'), securitysecurityQuestion: req.body.securityQuestion, securitysecurityAnswere: req.body.securityAnswere, validation:false, token: tokenRegister, endToken: moment().add(7, 'd').toDate(), firstLogin: true}
          dbo.collection(collectionUtenti).insertOne(myobj, function(err, res2) {
            if (err) throw err;
            mailVerification(req.body.mail, tokenRegister);
            console.log('Register: account registrato[' + req.body.mail + ':' + req.body.username + ']');
            res.send('Register: account registrato[' + req.body.mail + ':' + req.body.username + ']');
            db.close();
          });
        }
    });
    } else {
      console.log('Error:not same password ' + req.body.Password + ' - ' + req.body.rePassword);
      res.send('Error:not same password');
    }
  });
});
//validatemail(token [7d]) --> true, false
app.post('/db/validateAccount', function (req, res) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(databaseName);
    dbo.collection(collectionUtenti).findOne({token: req.body.token}, function(err, result) {
      if (result != null) {
        if (err) throw err;
        if (result.endToken.getTime() > moment().toDate().getTime()) {
          console.log('VerificateTime:tokenIsOk');
          var newtoken = randomString(30);
          var myquery = { token: req.body.token}; //result
          var newvalues = { $set: {   _id : result._id,
                                      mail : result.mail,
                                      password : result.password,
                                      token: newtoken,
                                      endToken: moment().add(5, 'h').toDate(),
                                      validation : true,
                                  }};
          dbo.collection(collectionUtenti).updateOne(myquery, newvalues, function(err, res) {
            if (err) throw err;
            console.log("Validation:validazione effettuata --> " + result.username);
            db.close();
          });

          res.send('TG:'+ newtoken);
        } else {
          console.log('Error:timeouttoken');
          res.send('Error:timeouttoken');
        }
        console.log('validateUser:' + result.mail);
        db.close();
      }
    });
  });
});
//loginUtente(username, password) --> true, false
app.post('/db/loginUtente', function (req, res) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(databaseName);
    var query = {username: req.body.username};
    dbo.collection(collectionUtenti).findOne(query, function(err, result) {
      if (err) throw err;
      if (result != null) {
        if (result.validation) {
          if (result.password == crypto.createHash('sha256').update(req.body.Password).digest('hex')) {
            var token = randomString(32);
            var newvalues = { $set: {token: token, endToken: moment().add(4, 'h').toDate()}};
            
            if(result.firstLogin){
              dbo.collection(collectionUtenti).updateOne({username: req.body.username}, newvalues, function(err, res) {
                if (err) throw err;
                  console.log('Log-in:' + result.username + ' | ' + token + ' | ' + moment(newvalues.endToken).format('h:m:ss:SSS'));
                  db.close();
                });
                res.send('FTG:' + token);
            } else {
              dbo.collection(collectionUtenti).updateOne({username: req.body.username}, newvalues, function(err, res) {
              if (err) throw err;
                console.log('Log-in:' + result.username + ' | ' + token + ' | ' + moment(newvalues.endToken).format('h:m:ss:SSS'));
                db.close();
              });
              res.send('TG:' + token);
            }

            
          } else {
            //password non corretta
            console.log('Error:password non valida');
            res.send('Error:password non valida');
          }
        } else {
          console.log('Error:Account non verificata');
          res.send('Error:Account non verificata');
        }
      } else {
        console.log('Error:Account non trovata');
        res.send('Error:mail non trovata');
      }
      db.close();
    });
  });
});
//forgotpassword(username, securityQuestion, securityAnswere, password, rePassword, callback) --> true, false
app.post('/db/forgotpassword', function (req, res) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(databaseName);
    if(req.body.password == req.body.rePassword){
      var query = {username: req.body.username, securitysecurityAnswere: req.body.securityAnswere, securitysecurityQuestion: req.body.securityQuestion};
      dbo.collection(collectionUtenti).findOne(query, function(err, result) {
        if (err) throw err;
        if (result != null) {
          if (result.validation) {
            //tutto controllato
            var newvalues = { $set: {   _id : result._id,
                username : result.username,
                password : crypto.createHash('sha256').update(req.body.Password).digest('hex'),
            }};
            dbo.collection(collectionUtenti).updateOne(query, newvalues, function(err, res) {
              if (err) throw err;
                console.log('password Reset:' + result.uername + ' | ' + req.body.rePassword);
                db.close();
            });
            res.send('password Reset:' + result.username + ' | ' + req.body.rePassword);
          } else {
            console.log('Error:Account non verificata');
            res.send('Error:Account non verificata');
          }
        } else {
          console.log('Error:Account non trovata');
          res.send('Error:Account non trovata');
        }
        db.close();
      });
    } else {
      console.log('Error:password not match');
      res.send('Error:password not match');
    }
  });
});
//Resetpassword(username, mail, password, rePassword, securityQuestion, Answer, token) --> true, false
app.post('/db/Resetpassword', function (req, res) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(databaseName);
    if (req.body.password == req.body.rePassword) {
      dbo.collection(collectionUtenti).findOne({username: req.body.username, securitysecurityQuestion: req.body.securityQuestion, securityAnswer: req.body.Answer, mail: req.body.mail, token : req.body.token}, function(err, result) {
            if (result != null) {
                var newtoken = randomString(30);
                var newvalues = { $set: {password: crypto.createHash('sha256').update(req.body.password).digest('hex'), token: newtoken}};
               
                dbo.collection(collectionUtenti).updateOne({mail: req.body.username}, newvalues, function(err, res) {
                if (err) throw err;
                    console.log('Reset password:' + result.token + ' | ' + newtoken );
                    db.close();
                });
                res.send('OK:' + newtoken);
            } else {
                console.log("Error:account not found");
                res.send("Error:account not found");;
            }
        });
    } else {
      console.log('Error:not same password');
      res.send('Error:not same password');
    }
  });
});
//ResetpasswordRequest(username, mail) --> true, false
app.post('/db/ResetpasswordRequest', function (req, res) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(databaseName);

    dbo.collection(collectionUtenti).findOne({username: req.body.username, mail: req.body.mail}, function(err, result) {
        if (result != null) {
            // send mail to the account request
            changepassword(req.body.mail, token);
            console.log('password: reset password request send | ' +  req.body.username);
            res.send('password: reset password request send | ' +  req.body.username);
        } else {
            // not found account with securityQuestion, answer, username
            console.log('Error: reset password request not found | ' +  req.body.username);
            res.send('Error: reset password request not found | ' +  req.body.username);
        }
    });

  });
});
//upgradetoken(username) --> true, false
app.post('/db/upgradetoken', function (req, res) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(databaseName);
    dbo.collection(collectionUtenti).findOne({username: req.body.username}, function(err, result) {
      if (result != null) {
        var newtoken = randomString(30);
        if (err) throw err;
        if (result.endToken.getTime() > result.token.getTime()) {

            var newvalues = { $set: {token: newtoken, endToken: moment().add(5, 'h').toDate()}};
            dbo.collection(collectionUtenti).updateOne({username: req.body.username}, newvalues, function(err, res) {
            if (err) throw err;
              db.close();
            });
            res.send('tokenChange:' + newtoken);


        } else {
            console.log('Error: timeouttoken');
            res.send('Error: timeouttoken');
        }
      } else {
        res.send('Error: not found token');
      }
    });
  });
});
//checktoken(username, token) --> true, false
app.post('/db/checktoken', function (req, res) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(databaseName);
    dbo.collection(collectionUtenti).findOne({username: req.body.username, token: req.body.token}, function(err, result) {
      if (result != null) {
        var newtoken = randomString(30);
        if (err) throw err;
        if (result.endToken.getTime() < moment().toDate().getTime()) {
            res.send('S:scaduto');
        } else {
            var newvalues = { $set: {token: newtoken, endToken: moment().add(5, 'h').toDate()}};
            dbo.collection(collectionUtenti).updateOne({username: req.body.username}, newvalues, function(err, res) {
            if (err) throw err;
              db.close();
            });
            res.send('TG:' + newtoken);
            console.log('TG: changetoken');
        }
      } else {
        res.send('Error: not found token');
      }
    });
  });
});
//addInformation(username, name, surname, birthDay, sentimental, musics, films) --> 
app.post('/db/addInformation', function (req, res) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(databaseName);
    dbo.collection(userInfo).findOne({username: req.body.username}, function(err, result) {
      if (result == null) {
        var myobj = {username: req.body.username, name: req.body.name, surname: req.body.surname, birthDay: req.body.birthDay, sentimental: req.body.sentimental, musics: req.body.musics, films: req.body.films}
        dbo.collection(userInfo).insertOne(myobj, function(err, res2) {
          if (err) throw err;
          console.log('Info: from[' + req.body.username + ']');
          res.send('Info: from[' + req.body.username + ']');
          db.close();
        });
      } else {
        res.send('Error: invalid data');
      }
    });
    dbo.collection(collectionUtenti).findOne({username: req.body.username}, function(err, result) {
      if (result != null) {
        var newvalues = { $set: {firstLogin: false}};
        dbo.collection(collectionUtenti).updateOne({username: req.body.username}, newvalues, function(err, res) {
        if (err) throw err;
        console.log('FirstLogin: ' + req.body.username);
          db.close();
        });
      } else {
        res.send('Error: invalid data');
      }
    });
  });
});
//getUser(token) --> username
app.post('/db/getUser', function (req, res) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(databaseName);
    dbo.collection(collectionUtenti).findOne({token: req.body.token}, function(err, result) {
      if (result != null) {
        res.send('GetUser: from|' + result.username);
        db.close();
      } else {
        res.send('Error: invalid data');
      }
    });
  });
});
//checkLogin(username) --> N if not logged
app.post('/db/checkLogin', function (req, res) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(databaseName);
    dbo.collection(collectionUtenti).findOne({token: req.body.token, username: req.body.username, firstLogin: false}, function(err, result) {
      if (result != null) {
        if(result.endToken.getTime() < moment().toDate().getTime()){
          res.send('N checkLogin');
          db.close();
        } else {
          var newtoken = randomString(30);

          var newvalues = { $set: {token: newtoken, endToken: moment().add(5, 'h').toDate()}};
          dbo.collection(collectionUtenti).updateOne({username: req.body.username}, newvalues, function(err, res) {
          if (err) throw err;
          db.close();
          });

          res.send('Y checkLogin: true|' + newtoken);
          db.close();
        }
      } else {
        res.send('N checkLogin');
        db.close();
      }
    });
  });
});
//post(username, text, date)
app.post('/db/setPost', function (req, res) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(databaseName);
    var myobj = {username: req.body.username , text: req.body.text, date: moment().toDate().getTime()}
    dbo.collection(posts).insertOne(myobj, function(err, res2) {
      if (err) throw err;
      res.send("posted")
      db.close();
    });
  });
})
//getAllPosts(scroll);
app.post('/db/getAllPosts', function (req, res) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(databaseName);
    var offset;
    var limite;
    if (req.body.scroll == 0) {
      offset = 0;
      limite = 20;
    } else {
      offset = 20 + (5 * (req.body.scroll - 1));
      limite = 5;
    }

    dbo.collection(posts).find({}).sort( { date: -1 } ).limit(limite).skip(offset).toArray(function(err, result) {
      if (err) throw err;
      res.send(result);
      db.close();
    });
  });
})
//getAllUsers;
app.post('/db/getStringUsers', function (req, res) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(databaseName);

    var s = '/^' + req.body.string + '/i ';

    dbo.collection(collectionUtenti).find({ username: { $regex: req.body.string, $options: 'i' } }).toArray(function(err, result) {
      if (err) throw err;
      res.send(result);
      db.close();
    });
  });
})
//getDefiniteUser(scroll, user)
app.post('/db/getDefiniteUser', function (req, res) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(databaseName);
    var offset;
    var limite;
    if (req.body.scroll == 0) {
      offset = 0;
      limite = 20;
    } else {
      offset = 20 + (5 * (req.body.scroll - 1));
      limite = 5;
    }

    dbo.collection(posts).find({username: req.body.user}).sort( { date: -1 } ).limit(limite).skip(offset).toArray(function(err, result) {
      if (err) throw err;
      res.send(result);
      db.close();
    });
  });
})
//checkIffollow(username, account)
app.post('/db/checkIfFollow', function (req, res) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(databaseName);

    dbo.collection(follower).find({username: req.body.username, account: req.body.account}).toArray(function(err, result) {
      if (err) throw err;
      if (result.length == 0){
        res.send('F');
      } else {
        res.send('U');
      }
      db.close();
    });
  });
})
//FollowUnfollow(username, account)
app.post('/db/FollowUnfollow', function (req, res) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(databaseName);
    var myobj = { username: req.body.username, account: req.body.account, date: moment().toDate()};
    var query = { username: req.body.username, account: req.body.account };

    dbo.collection(follower).findOne(query, function(err, result) {
      if (result == null){
        dbo.collection(follower).insertOne(myobj, function(err, res) {
          console.log(req.body.username + ' start following ' + req.body.account);
          if (err) throw err;
          //res.send('F');
          db.close();
        });
      } else {
        dbo.collection(follower).deleteOne(query, function(err, res) {
          console.log(req.body.username + ' stop following ' + req.body.account);
          if (err) throw err;
          //res.send('U');
          db.close();
        });
      }
    });
    res.send();
  }); 
});
//getFollowerArray(username)
app.post('/db/ArrayFollower', function (req, res) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(databaseName);
    
    dbo.collection(follower).find({ username: req.body.username }).toArray(function(err, resultONE) {
      res.send(resultONE);
    });
  }); 
})
//FollowerPosts(array, numberPagePost, number)
app.post('/db/FollowerPosts', function (req, res) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    console.log("ASD");
    var dbo = db.db(databaseName);
    var offset;
    var limite;
    var initialPost = req.body.numberPagePost / 2 * 3;

    if (req.body.number == 0) {
      offset = 0;
      limite = initialPost;
    } else {
      offset = initialPost + (req.body.numberPagePost * (req.body.number - 1));
      limite = req.body.numberPagePost;
    }

    dbo.collection(posts).find({ username: { $in: req.body.array } }).sort( { date: -1 } ).limit(limite).skip(offset).toArray(function(err, resultONE) {
      console.log("Limite: " + limite + "; Offset: " + offset);
      res.send(resultONE);
    });
  }); 
});
//getNotifiche(username)
app.post('/db/getNotifiche', function (req, res) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(databaseName);
    var query = { to: req.body.username, read: false };
    dbo.collection(messages).find(query).toArray(function(err, result) {
      var num = result.length;
      res.send(num.toString());
    });
  }); 
});
//sendMessage(from, to, text, url)
app.post('/db/sendMessage', function (req, res) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(databaseName);
    var myobj = {read: false, from: req.body.from , to: req.body.to, text: req.body.text, url: req.body.url, date: moment().toDate()};
    dbo.collection(messages).insertOne(myobj, function(err, res2) {
      if (err) throw err;
      res.send('S');
      console.log('Message --> [' + req.body.from + ']: ' + req.body.text + ' [' + req.body.to + ']');
      db.close();
    });
  });
});
//getChats(username)
app.post('/db/getChats', function (req, res) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(databaseName);
    var query = { to: req.body.username, read: false };
    var array = [];
    dbo.collection(messages).find(query).toArray(function(err, result) {
      for(var i = 0; i < result.length; i++){
        array.push(result[i].from);
      }
      
      res.send(array);
    });
  }); 
});
//loadNonReadMessages(username, reciever)
app.post('/db/loadNonReadMessage', function (req, res) {
  MongoClient.connect(url, function(err, db) {
    if (err) throw err;
    var dbo = db.db(databaseName);
    var sort = { date: -1 };
    var query = { to: req.body.username, read: false };
    var query = { $or: [ { to: req.body.username, read: false }, { from: req.body.username} ] }
    var array = [];
    dbo.collection(messages).find(query).sort(sort).toArray(function(err, result) {
      for(var i = 0; i < result.length; i++){
        array.push(result[i].from);
      }
      
      res.send(array);
    });
  }); 
});

app.use(express.static('assets'));
app.listen(PORT, () => {
});
